//
//  Utilidades.m
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 28/8/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import "Utilities.h"
#import "partituraDTO.h"


@implementation Utilities


+ (NSMutableArray *)getLocalMusicInfo{
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * documentsPath = [resourcePath stringByAppendingPathComponent:@"xmlLocal"];
    
    NSError * error;
    NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsPath error:&error];
    
    int count;
    for (count = 0; count < (int)[directoryContents count]; count++) {
        
        partituraDTO *p = [[partituraDTO alloc] init];
        
        NSString* titulo = [directoryContents objectAtIndex:count];
        long count_libreria = [titulo length]; //numero de caracteres en el título
        titulo = [titulo substringToIndex:count_libreria - 4]; //titulo quitando caracteres .xml
        
        p.title = titulo;
        p.path = [directoryContents objectAtIndex:count];
        
        NSString* scorePath = [NSString stringWithFormat:@"%@/%@", documentsPath, p.path];
        p.path = scorePath;
        char path[255];
        [scorePath getCString:path maxLength:255 encoding:NSUTF8StringEncoding];
        
        p.isLocal = TRUE;
        
        [arr addObject:p];
        
    }
    return arr;
}

+ (NSMutableArray *)getPartPicturesName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:@"/bbpartpictures/"]; //add our image to the path
    
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    /*NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * documentsPath = [resourcePath stringByAppendingPathComponent:@"bbpartpictures"];
    */
    NSError * error;
    NSArray * directoryContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:fullPath error:&error];
    
    int count;
    for (count = 0; count < (int)[directoryContents count]; count++) {
        
        [arr addObject:[directoryContents objectAtIndex:count]];
    }
    return arr;
}

@end
