//
//  ReadScoreOC.h
//
//  You are free to copy and modify this code as you wish
//  No warranty is made as to the suitability of this for any purpose
//
// Objective-C API for ReadScoreLib

#ifndef ReadScoreOC_h
#define ReadScoreOC_h

#include "rscore.h"
#import <UIKit/UIKit.h>

@interface ReadScoreOptions : NSObject

@property (readonly) rscore_options rawoptions;

-(instancetype)initWith:(NSString*)softwareName flags:(unsigned)flags ocr_data:(NSString*)ocr_data_path;

@end

@interface RSBarline : NSObject
@property (readonly) CGPoint base;
@property (readonly) CGFloat height;
@end

@interface RSBarInfo : NSObject
@property (readonly) RSBarline *leftBarline;
@property (readonly) RSBarline *rightBarline;
@property (readonly) CGRect barRect;
@property (readonly) int startBeat;
@property (readonly) int numBeats;
@property (readonly) unsigned flags;
@property (readonly) float cantus_offset; // [0.0 .. 1.0]
@property (readonly) int page;
@end

typedef void (^progressHandler_t)(float info);
typedef void (^completionHandler_t)(NSArray<RSBarInfo*>* barsInfo);
typedef UIImage* (^getNextImageHandler_t)(void); // returns nil when no more images

@interface ReadScoreOC : NSObject

+ (NSString*)versionString;

- (instancetype)initWithOptions:(ReadScoreOptions*)options midiPath:(NSString*)midiPath xmlPath:(NSString*)xmlPath;

// starts processing on background thread. Calls completion on main thread
-(void)processImage:(UIImage*)image progress:(progressHandler_t)progress completion:(completionHandler_t)completion;

@end

#endif /* ReadScoreOC_h */
