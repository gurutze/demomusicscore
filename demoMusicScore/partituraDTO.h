//
//  partituraDTO.h
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 28/8/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface partituraDTO : NSObject

@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSString *editorial;
@property (nonatomic, retain) NSString *idPart;
@property (nonatomic, retain) NSString *instrument;
@property (nonatomic, retain) NSString *path;
@property (nonatomic) char loaderPath;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *birthDate;
@property (nonatomic, retain) NSString *deadDate;
@property (nonatomic, retain) NSString *catalogue;
@property (nonatomic, retain) NSString *arrangements;
@property (nonatomic, retain) NSString *transcriptions;
@property (nonatomic, retain) NSString *musicalForm;
@property (nonatomic, retain) NSString *genre;
@property (nonatomic, retain) NSString *instrumentation;
@property (nonatomic, retain) NSString *period;
@property (nonatomic) BOOL isLocal;

- (id)initWithDict: (NSDictionary *)initDict;
@end
