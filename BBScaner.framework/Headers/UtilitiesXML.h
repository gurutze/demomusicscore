//
//  UtilitiesXML.h
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 7/9/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilitiesXML : NSObject

- (NSDictionary *)deXMLaDictFromPath: (NSString *) initPath;
- (BOOL)processArrayXML: (NSArray *) initArrayXml andSaveWithName: (NSString *) initName;
- (BOOL) checkIsSameScoreBetweenScore1: (NSDictionary *) score1 andScore2: (NSDictionary *) score2;
- (NSString *)deDictAXml: (NSDictionary *) initDict;
@end
