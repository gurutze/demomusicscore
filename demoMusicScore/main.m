//
//  main.m
//  demoMusicScore
//
//  Created by Jose Cruz Perez Pi on 25/1/18.
//  Copyright © 2018 newmusicnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
