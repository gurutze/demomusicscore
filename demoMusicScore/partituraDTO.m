//
//  partituraDTO.m
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 28/8/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import "partituraDTO.h"

@implementation partituraDTO
@synthesize author, editorial, idPart, instrument, path, title, birthDate, deadDate, catalogue, arrangements, transcriptions, musicalForm, genre, instrumentation, period, isLocal, loaderPath;

- (id)initWithDict: (NSDictionary *)initDict{
    
    author = [initDict objectForKey:@"author"];
    editorial = [initDict objectForKey:@"editorial"];
    idPart = [initDict objectForKey:@"id"];
    instrument = [initDict objectForKey:@"instrument"];
    path = [initDict objectForKey:@"path"];
    title = [initDict objectForKey:@"title"];
    birthDate = [initDict objectForKey:@"birthDate"];
    deadDate = [initDict objectForKey:@"deadDate"];
    catalogue = [initDict objectForKey:@"catalogue"];
    arrangements = [initDict objectForKey:@"arrangements"];
    transcriptions = [initDict objectForKey:@"transcriptions"];
    musicalForm = [initDict objectForKey:@"musicalForm"];
    genre = [initDict objectForKey:@"genre"];
    instrumentation = [initDict objectForKey:@"instrumentation"];
    period = [initDict objectForKey:@"period"];
    
    
    return self;
}

@end
