//
//  Utilidades.h
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 28/8/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Utilities : NSObject

+ (NSMutableArray *)getLocalMusicInfo;
+ (NSMutableArray *)getPartPicturesName;

@end
