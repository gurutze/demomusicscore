//
//  PhotoViewController.h
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 7/9/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cellImagenes.h"
#import <BBSCaner/BBScaner-Swift.h>
#import <BBScoreViewer/BBScoreViewerViewController.h>

@interface PhotoViewController : UIViewController <BBScoreViewerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, cellDelegate, UICollectionViewDelegate>{
    NSInteger contador;
    NSInteger indice;
    IBOutlet UICollectionView *collection;
    NSMutableArray *arrPicturesSaved;
    BBScanerManager *bbScaner;
    BBScoreViewerViewController *bbscoreviewer;
}


@property (nonatomic, retain) UIImagePickerController *imagePicker;

- (IBAction)PulsarCamera:(id)sender;
- (IBAction)PulsarExportarXML:(id)sender;

@end
