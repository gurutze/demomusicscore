//
//  cellImagenes.m
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 7/9/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import "cellImagenes.h"

@implementation cellImagenes
@synthesize imgCelda;

- (void)deleteAction:(id)sender {
    if([self.delegate respondsToSelector:@selector(deleteAction:forCell:)]) {
        [self.delegate deleteAction:sender forCell:self];
    }
}


// Must implement this method either here or in the UIViewController
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    if (action == @selector(deleteAction:)) {
        return YES;
    }
    
    return NO;
}

@end
