//
//  BBScaner.h
//  BBScaner
//
//  Created by Jose Cruz Perez Pi on 31/1/18.
//  Copyright © 2018 newmusicnow. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BBScaner.
FOUNDATION_EXPORT double BBScanerVersionNumber;

//! Project version string for BBScaner.
FOUNDATION_EXPORT const unsigned char BBScanerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BBScaner/PublicHeader.h>
#import <BBSCaner/ReadScoreOC.h>
#import <BBSCaner/rscore.h>
#import <BBSCaner/UtilitiesXML.h>
