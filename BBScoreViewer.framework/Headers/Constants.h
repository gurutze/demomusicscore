//
//  Constants.h
//  BBScoreViewer
//
//  Created by Jose Cruz Perez Pi on 1/2/18.
//  Copyright © 2018 newmusicnow. All rights reserved.
//

#define MONITORING_OPEN_SCORE @"open_score"
#define MONITORING_CLOSE_SCORE @"close_score"
#define MONITORING_START_PS @"start_ps"
#define MONITORING_STOP_PS @"stop_ps"
#define MONITORING_LAST_STAFF @"last_staff"
#define MONITORING_END_ANOTTATIONS @"end_annotations"
#define MONITORING_ANOTTATION @"annotation"
