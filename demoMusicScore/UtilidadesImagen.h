//
//  UtilidadesImagen.h
//  
//
//  Created by josecruzperez on 22/10/14.
//  Copyright (c) 2014 Redsys. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UtilidadesImagen : NSObject


+ (void)saveImage:(UIImage*)image imagen:(NSString*)imageName;
+ (void)removeImage:(NSString*)fileName;
+ (UIImage*)loadImage:(NSString*)imageName;
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize;
+ (UIImage *) imagenAjustada: (UIImage *) image;
+ (UIImage*)RotarImagen: (UIImage *)imagen_a_rotar Grados: (int)grados;
+ (UIImage *) generateQRCodeWithString:(NSString *)string;
@end
