//
//  cellImagenes.h
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 7/9/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import <UIKit/UIKit.h>

@class cellImagenes;

@protocol cellDelegate <NSObject>

@optional
- (void)deleteAction:(id)sender forCell:(cellImagenes *)cell;
@end

@interface cellImagenes : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *imgCelda;
@property (weak, nonatomic) id<cellDelegate> delegate;
@end
