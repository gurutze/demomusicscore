//
//  rscore.h
//	definition of C interface to ORT
//
//  ReadScoreLib
//

#ifndef READSCORELIB_rscore_h
#define READSCORELIB_rscore_h

#ifdef __cplusplus
extern "C" {
#else
#include <stdbool.h>
#endif
	
#ifdef _WIN_
#define EXPORT	__declspec( dllexport )
#else
#define EXPORT
#endif

	/*!
	 @header	rscore.h
	 
	 @abstract	The base C interface to ReadScoreLib<br>
	 
	 This interface provides functions to read a 32bit pixelmap image and convert to MusicXML or MIDI
	 
	 The user will:
	 1) convert the single page image to an array of 32-bit RGBA pixels. Alpha is expected to be FF (future TBD).
	 2) pass the image data, width, height, orientation, and required MIDI, XML file paths to rscore_convert on 
	 a background thread as it can take many seconds
	 3) handle progress calls to the callback, returning false if the user cancels
	 4) if rscore_convert returns no error, MIDI and XML file can be found and used
	 5) the rscore returned can be used to find bar positions using rscore_getnumbars and rscore_getbarinfo
	 6) rscore must be deleted using rscore_delete 
	 */
	
	static const int rscore_kMaxLenSoftwareName = 64;
	static const int rscore_kMaxPathLen = 1024;

	/*! @typedef rscore
		@abstract the score reader
	 */
	typedef struct rscore rscore;


	/*! @enum rscore_error
		@abstract all errors
	 */
	enum rscore_error
	{
		rscore_NoError = 0,
		rscore_UnlicensedFunctionError,
		rscore_OutOfMemoryError,
		rscore_FileOpenFailedError,
		rscore_FailedError,
		rscore_FailedTooManyStaffsError,
		rscore_FunctionNotImplementedError,
		rscore_ReentrantFailError
	};
	
	/*! @enum rscore_imageorientation
		@abstract Rotation required to make image the right way up
		ie the way up the image was through the viewfinder
	 */
	enum rscore_imageorientation {
		rscore_eROT0 = 1, // values as EXIF
		rscore_eROT90 = 8,
		rscore_eROT180 = 3,
		rscore_eROT270 = 6
	};

	/*! @enum rscore_optionsflags
		@abstract option flags for rscore_options.flags
		@discussion these values are actual bits
	 */
	enum rscore_optionsflags {
		// Ignore all staffs above the lower two, taken system by system
		rscore_excludeUpperStaff_flag				=	0x0001,
		// Ignore small gauge chamber staffs - not implemented
		rscore_excludeSmallUpperStaffs_flag			=	0x0002,
		// recognise tremolo notations
		rscore_optParseTremolo						=	0x0004,
		// generate the equivalent non-tremolo notation for recognised tremolo
		// *** leave flag unset to generate tremolo notation in output MusicXML ***
		rscore_optTremoloExpand						=	0x0010,
		// Generate MusicXML Default x, y
		rscore_xmlDefaultCoords_flag				=	0x0100,
		// Generate a text file listing the bounds of objects
		rscore_xmlObjectBounds_flag					=	0x0200,
		// guess transposing instruments
		rscore_optTransposingInstruments			=	0x0400,
		// unpublished
		rscore_optBarlineDetectionSensitive			=	0x0800,
		// restrict operation to image processing
		rscore_optLens								=	0x1000,
		// parse french (over staff) time signatures
		rscore_optFrenchTimeSignatures				=	0x4000
	};

	enum rscore_barflags {
		rscore_BEYOND_SCORES		=	0x0001, // internal use only
		rscore_FIRST_OF_SYSTEM		=	0x0002, // marks the leftmost bar in the system
		rscore_INVISIBLE			=	0x0004, // internal use only
		rscore_YERROR				=	0x0008, // internal use only
		rscore_EOSCORE				=	0x0010, // marks the last bar in the score
		rscore_SPLITBAR_L			=	0x0020, // box encloses the part of the bar at the end of one system
		rscore_SPLITBAR_R			=	0x0040, // box encloses the part of the bar at the beginning of the next system
		rscore_firstOfSection		=	0x0080  // bar is the first of a movement or a musical section
	};

	/*! @struct rscore_version
	@abstract a version number
	*/
	typedef struct rscore_version
	{
		int major;
		int minor;
	} rscore_version;

	/*! @struct rscore_options
	 @abstract options for rscore_convert
	 @discussion software = name of the software including version number (null-terminated) which is added to the XML file in the identification element
	*/
	typedef struct rscore_options
	{
		char software[rscore_kMaxLenSoftwareName];
		unsigned flags; // default is 0. use rscore_optionsflags bits
        char data_path[rscore_kMaxPathLen];
		unsigned long midi_staff_mask;	// internal use only, must be zero
		unsigned dummy[62];	// for future. set to zero
	} rscore_options;


	/*! @class rscore_errorinfo
	@abstract error information
	*/
	typedef struct rscore_errorinfo
	{
		enum rscore_error err;
		unsigned dummy[32];// for future. set to 0
	} rscore_errorinfo;
	
	/*! @class rscore_convert_progressinfo
		@abstract information about the progress of rscore_convert
	 */
	typedef struct rscore_convert_progressinfo
	{
		int progress_percent; // [0..100]
	} rscore_convert_progressinfo;

	/*! @class rscore_pos
		@abstract a position
	 */
	typedef struct rscore_pos
	{
		int x,y;
	} rscore_pos;
	
	/*! @class rscore_barline
		@abstract a barline placement
	 */
	typedef struct rscore_barline
	{
		rscore_pos base;	// barline base
		int height;			// barline height
	} rscore_barline;

	/*! @class rscore_barinfo
		@abstract information about a bar
	 */
	typedef struct rscore_barinfo
	{
		rscore_barline left_barline;	// position of opening barline
		rscore_barline right_barline;	// position of closing barline
		int startbeat;					// the starting beat number of this bar
		int numbeats;					// the number of beats in this bar
		unsigned flags;					// set of bits defined by rscore_barflags
		int cantus_offset;				// percentage of bar to the left of the first cantus
		int page;						// page bearing the opening barline
		unsigned dummy[31];				// for future. set to 0
	} rscore_barinfo;
	
	/*!
	 * for use with rscore_optLens only
	 */
	typedef struct rscore_lensimage
	{
		const unsigned *data;
		int width, height;
	} rscore_lensimage;

	/*!
	 * internal debug use only
	 */
	typedef struct rscore_debugimage
	{
		const unsigned *data;
		int width, height;
	} rscore_debugimage;
	
	/*!
	@function rscore_getversion
	@abstract get the version number of this library
	@discussion when rendering this as a string the minor number must have 2 digits, ie it needs
	 a leading zero when < 10
	@returns the version as major and minor integers.
	*/
	EXPORT rscore_version rscore_getversion(void);
	
	/*!
	 @function rscore_getversionstr
	 @abstract get the version number of this library as a string with correct 0 padding
	 @returns the version as a string
	 */
	EXPORT const char *rscore_getversionstr(void);

	/*!
	 @typedef rscore_convert_callback_fn
	 @abstract The type of a callback called from rscore_convert
	 @discussion The callback is called by rscore_convert to monitor progress.
	 This allows the ui to update while the layout is in progress
	 The callback returns false to abort the conversion and rapid return from rscore_convert, else true.
	 @param arg the context argument from rscore_convert
	 @return false to abort the conversion
	 */
	typedef bool (*rscore_convert_callback_fn)(const rscore_convert_progressinfo *info, void *arg);
	
	/*!
	 @function rscore_convert
	 @abstract read the image (32bpp RGBA) described by data, width, height and convert to MIDI or XML files
	 NB This function blocks until complete so should be called on a background thread
	 cb will be called regularly to update progress indication, and perhaps (in future) provide partial results
	 @param data array of 32bit pixels
	 @param imageWidth the pixel width of the image
	 @param imageHeight the pixel height of the image
	 @param rowbytes the byte offset from one row to the next and must be a multiple of 4
	 @param orient the orientation of the image
	 @param cb the callback to be called at regular intervals to report progress
	 @param arg the context argument to be passed to cb
	 @param midifilepath the full pathname to save the MIDI data to. If NULL MIDI is not saved
	 @param xmlfilepath the full pathname to save the MusicXML data to. If NULL XML is not saved
	 @param options options
	 @param err if non-null points to a struct which receives any error information
	 @return rscore pointer which the caller should delete with rscore_delete
	 */
	EXPORT rscore *rscore_convert(const unsigned *data, int imageWidth, int imageHeight, int rowbytes, enum rscore_imageorientation orient,
								  rscore_convert_callback_fn cb, void *arg,
								  const char *midifilepath, const char *xmlfilepath,
								  const rscore_options *options,
								  rscore_errorinfo *err);

	/*!
	 @function rscore_fconvert		-	batch convertion from file(s)
	 @abstract read one or more page image files and output corresponding MIDI and MusicXML files 
	 NB This function blocks until complete so should be called on a background thread
	 cb will be called regularly to report progress
	 @param buildfilepath			-	single image file: the full path to the image file
										one or more image files: buildfilepath gives the full path to a build file containing a list of image files on consecutive lines and terminated by a *
										the names image files should reside in the same directory as the build file
	 @param midifilepath			-	if non-null the MIDI file is written to the full path midifilepath.  Otherwise it is written to the build file directory
	 @param xmlfilepath				-	if non-null the MusicXML file is written to the full path xmlfilepath.  Otherwise it is written to the build file directory
	 @param cb						-	user-supplied callback called periodically to report progress
	 @param arg						-	the context argument to be passed to cb
	 @param options options			-	information including bits specifing selected options (see rscore_optionsflags)
	 @param err						-	if non-null points to a struct which receives any error information
	 @return						-	handle to rscore instance.  handle should be deleted on completion with rscore_delete
	 */
	EXPORT rscore *rscore_fconvert(const char *buildfilepath, const char *midifilepath, const char *xmlfilepath, rscore_convert_callback_fn cb, void *arg, rscore_options *options, rscore_errorinfo *err);

	/*!
	 @function rscore_getlensimage
	 @param rsc the rscore
	 @return information for retrieval of the image which has the same dimensions as the input image
	 */
	EXPORT rscore_lensimage rscore_getlensimage(const rscore *rsc);

	/*!
	 @function rscore_delete
	 @abstract delete the pointer returned by rscore_convert
	 */
	EXPORT void rscore_delete(rscore *rsc);

	/*!
	 @function rscore_getnumbars
	 @return the number of bars found in the score
	 */
	EXPORT int rscore_getnumbars(const rscore *rsc);
	/*!
	 @function rscore_getbarinfo
	 @param rsc the rscore
	 @param barindex the index of the bar [0..rscore_getnumbars-1]
	 @return information about a particular bar
	 */
	EXPORT rscore_barinfo rscore_getbarinfo(const rscore *rsc, int barindex);
	/*!
	 @function rscore_getstaffcount
	 @return the effective number of staffs in the score
	 */
	EXPORT int rscore_getstaffcount(const rscore *rsc);
	
	/*!
	 * internal debug use only
	 */
	rscore_debugimage rscore_getdebugimage(const rscore *rsc);

#ifdef __cplusplus
}
#endif

#endif
