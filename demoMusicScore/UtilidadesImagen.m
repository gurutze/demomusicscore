//
//  UtilidadesImagen.m
//  
//
//  Created by josecruzperez on 22/10/14.
//  Copyright (c) 2014 Redsys. All rights reserved.
//

#import "UtilidadesImagen.h"
#include <math.h>


@implementation UtilidadesImagen

static inline double radians (double degrees) {return degrees * M_PI/180;}

+ (void)saveImage:(UIImage*)image imagen:(NSString*)imageName {
    
    NSData *imageData = UIImagePNGRepresentation(image); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/bbpartpictures"];
    
    NSError *error;
    if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
        [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
    
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/bbpartpictures/%@.png", imageName]]; //add our image to the path
    NSLog(@"full path = %@", fullPath);
    
    [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
    
    NSLog(@"image saved");
    
}

+ (void)removeImage:(NSString*)fileName {
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/bbpartpictures/%@", fileName]];
    
    [fileManager removeItemAtPath: fullPath error:NULL];
    
    NSLog(@"image removed");
    
}


+ (UIImage*)loadImage:(NSString*)imageName {
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"/bbpartpictures/%@", imageName]];
    NSLog(@"full path = %@", fullPath);
    
    return [UIImage imageWithContentsOfFile:fullPath];
    
}

+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *) imagenAjustada: (UIImage *) image{
    
    int resta;
    resta=image.size.height-image.size.width;
    NSLog(@"%i",resta);
    
    if (resta<0){
        
        CGSize medida=image.size;
        
        image=[UtilidadesImagen imageWithImage:[self RotarImagen:image Grados:0] scaledToSize:medida];
        
        UIImage *bottomImage = [[UIImage alloc]init];
        
        CGRect rect=CGRectMake(0, 0, image.size.height, image.size.height);
        
        
        UIGraphicsBeginImageContext(rect.size);
        
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        [bottomImage drawInRect:CGRectMake(0,0,image.size.height,image.size.height)];
        
        CGContextTranslateCTM(currentContext, 0.0, rect.size.height);
        CGContextScaleCTM(currentContext, 1.0, -1.0);
        
        CGRect clippedRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
        CGContextClipToRect( currentContext, clippedRect);
        
        CGRect drawRect = CGRectMake(resta/2,rect.origin.y,image.size.width,image.size.height);
        NSLog(@"%f", image.size.width);
        NSLog(@"%f", image.size.height);
        CGContextDrawImage(currentContext, drawRect, image.CGImage);
        CGContextScaleCTM(currentContext, 1.0, -1.0);
        
        UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return [UtilidadesImagen imageWithImage:cropped scaledToSize:image.size];
        
        
    }
    else if (resta>0){
        
        resta=resta*(-1);
        NSLog(@"%i", resta);
        
        CGSize medida=image.size;
        if (image.imageOrientation == UIImageOrientationDown) {
            NSLog(@"image orientation left");
            image=[UtilidadesImagen imageWithImage:[self RotarImagen:image Grados:90] scaledToSize:medida];
            
        }
        if (image.imageOrientation == UIImageOrientationRight) {
            NSLog(@"image orientation left");
            image=[UtilidadesImagen imageWithImage:[self RotarImagen:image Grados:90] scaledToSize:medida];
            
        }
        if (image.imageOrientation == UIImageOrientationLeft) {
            NSLog(@"image orientation left");
            image=[UtilidadesImagen imageWithImage:[self RotarImagen:image Grados:-90] scaledToSize:medida];
            
        }
        
        UIImage *bottomImage = [[UIImage alloc]init];
        
        CGRect rect=CGRectMake(0, 0, image.size.width, image.size.width);
        
        
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        
        [bottomImage drawInRect:CGRectMake(0,0,image.size.width,image.size.width)];
        
        CGContextTranslateCTM(currentContext, 0.0, rect.size.height);
        CGContextScaleCTM(currentContext, 1.0, -1.0);
        
        CGRect clippedRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
        CGContextClipToRect( currentContext, clippedRect);
        
        int resta_horizontal=(image.size.width-image.size.height)/2;
        CGRect drawRect = CGRectMake(rect.origin.x,resta_horizontal,image.size.width,image.size.height);
        
        CGContextDrawImage(currentContext, drawRect, image.CGImage);
        CGContextScaleCTM(currentContext, 1.0, -1.0);
        
        UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
        
        
        
        UIGraphicsEndImageContext();
        
        return [UtilidadesImagen imageWithImage:cropped scaledToSize:image.size];
        
        
    }
    else if (resta==0){
        
        
        UIImage *bottomImage = [[UIImage alloc]init];
        
        CGRect rect=CGRectMake(0, 0, image.size.width, image.size.width);
        
        
        UIGraphicsBeginImageContext(rect.size);
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        
        [bottomImage drawInRect:CGRectMake(0,0,image.size.width,image.size.width)];
        
        CGContextTranslateCTM(currentContext, 0.0, rect.size.height);
        CGContextScaleCTM(currentContext, 1.0, -1.0);
        
        CGRect clippedRect = CGRectMake(0, 0, rect.size.width, rect.size.height);
        CGContextClipToRect( currentContext, clippedRect);
        
        
        CGRect drawRect = CGRectMake(0,rect.origin.y,image.size.width,image.size.height);
        
        CGContextDrawImage(currentContext, drawRect, image.CGImage);
        CGContextScaleCTM(currentContext, 1.0, -1.0);
        
        UIImage *cropped = UIGraphicsGetImageFromCurrentImageContext();
        
        
        
        UIGraphicsEndImageContext();
        
        return [UtilidadesImagen imageWithImage:cropped scaledToSize:image.size];
        
        
        
    }
    else{
        return nil;
    }
    
}

+ (UIImage*)RotarImagen: (UIImage *)imagen_a_rotar Grados: (int)grados{
    
    if (grados==180){
        UIGraphicsBeginImageContext(imagen_a_rotar.size);
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        CGContextTranslateCTM( currentContext, 0.5f * imagen_a_rotar.size.width, 0.5f * imagen_a_rotar.size.height ) ;
        CGContextRotateCTM( currentContext, radians( grados ) ) ;
        
        CGContextScaleCTM(currentContext, 1.0, -1.0);
        CGContextDrawImage(currentContext, CGRectMake(-imagen_a_rotar.size.width / 2, -imagen_a_rotar.size.height / 2, imagen_a_rotar.size.width, imagen_a_rotar.size.height), [imagen_a_rotar CGImage]);
        
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
    
    else if ((grados==90)||(grados==-90)){
        UIGraphicsBeginImageContext(CGSizeMake(imagen_a_rotar.size.height,imagen_a_rotar.size.width));
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        CGContextTranslateCTM( currentContext, 0.5f * imagen_a_rotar.size.height, 0.5f * imagen_a_rotar.size.width ) ;
        CGContextRotateCTM( currentContext, radians( grados ) ) ;
        
        CGContextScaleCTM(currentContext, 1.0, -1.0);
        CGContextDrawImage(currentContext, CGRectMake(-imagen_a_rotar.size.width / 2, -imagen_a_rotar.size.height / 2, imagen_a_rotar.size.width, imagen_a_rotar.size.height), [imagen_a_rotar CGImage]);
        
        
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
        
    }
    else{
        return imagen_a_rotar;
    }
}

+ (UIImage *) generateQRCodeWithString:(NSString *)string{
    NSData *stringData = [string dataUsingEncoding:NSUTF8StringEncoding ];
    
    CIFilter *filter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [filter setValue:stringData forKey:@"inputMessage"];
    [filter setValue:@"M" forKey:@"inputCorrectionLevel"];
    
    // Render the image into a CoreGraphics image
    CGImageRef cgImage = [[CIContext contextWithOptions:nil] createCGImage:[filter outputImage] fromRect:[[filter outputImage] extent]];
    
    //Scale the image usign CoreGraphics
    UIGraphicsBeginImageContext(CGSizeMake([[filter outputImage] extent].size.width * 100.0, [filter outputImage].extent.size.width * 100.0));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetInterpolationQuality(context, kCGInterpolationNone);
    CGContextDrawImage(context, CGContextGetClipBoundingBox(context), cgImage);
    UIImage *preImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //Cleaning up .
    UIGraphicsEndImageContext();
    CGImageRelease(cgImage);
    
    // Rotate the image
    UIImage *qrImage = [UIImage imageWithCGImage:[preImage CGImage]
                                           scale:[preImage scale]
                                     orientation:UIImageOrientationDownMirrored];
    return qrImage;
}

@end
