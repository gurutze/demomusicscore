//
//  PhotoViewController.m
//  editorBB
//
//  Created by Jose Cruz Perez Pi on 7/9/17.
//  Copyright © 2017 newmusicnow. All rights reserved.
//

#import "PhotoViewController.h"
#import "UtilidadesImagen.h"
#import "Utilities.h"
#import "cellImagenes.h"

@interface PhotoViewController ()

@end

@implementation PhotoViewController
@synthesize imagePicker;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect targetRectangle = CGRectMake(100, 100, 100, 100);
    [[UIMenuController sharedMenuController] setTargetRect:targetRectangle
                                                    inView:self.view];
    
    UIMenuItem *menuItem = [[UIMenuItem alloc] initWithTitle:@"Borrar"
                                                      action:@selector(deleteAction:)];
    
    [[UIMenuController sharedMenuController] setMenuItems:[NSArray arrayWithObjects:menuItem, nil]];
    
    arrPicturesSaved = [[NSMutableArray alloc] init];
    arrPicturesSaved = [Utilities getPartPicturesName];
    
    contador = [arrPicturesSaved count];
    
    [collection registerNib:[UINib nibWithNibName:@"cellImagenes" bundle:nil] forCellWithReuseIdentifier:@"cellImagenes"];
    
    [self openCamera];
    bbScaner = [[BBScanerManager alloc]init];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) openCamera{
 
    imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.allowsEditing=NO;
    
    imagePicker.showsCameraControls = YES;
    [self performSelectorOnMainThread:@selector(showPicker) withObject:nil waitUntilDone:NO];
}

- (void) showPicker{
    

    [self presentViewController:imagePicker animated:YES completion:nil];
    
}

- (IBAction)PulsarCamera:(id)sender{
    
    [self openCamera];
}

- (IBAction)PulsarExportarXML:(id)sender{
  
    //Convertir una imagen
    
    [bbScaner getXmlFromImageWithImage:[UIImage imageNamed:@"pruebaImagen"] imageName:@"pruebaImagen" completion:^(NSString * result) {
        NSLog(@"result = %@",result);
        bbscoreviewer = [[BBScoreViewerViewController alloc] initWithScore:result andCustomization:@"" andOrigin:0];
        [bbscoreviewer setDelegate:self];
        [self presentViewController:bbscoreviewer animated:NO completion:nil];
    }];
    
    
    //Convertir un array de imágenes
    /*
    NSMutableArray *images = [[NSMutableArray alloc] init];
    for (int i = 0; i<[arrPicturesSaved count]; i++){
        [images addObject:[UtilidadesImagen loadImage:[arrPicturesSaved objectAtIndex:i]]];
    }
    [bbScaner getXmlFromImagesWithImages:[NSArray arrayWithArray:images] exitFileName:@"PEPE" completion:^(NSString * result) {
        NSLog(@"result = %@",result);
        
        bbscoreviewer = [[BBScoreViewerViewController alloc] initWithScore:result andCustomization:@"" andOrigin:0];
        [bbscoreviewer setDelegate:self];
        [self presentViewController:bbscoreviewer animated:NO completion:nil];
    }];
    */
    
    
    //Convertir un pdf
    /*
    NSString * resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString * documentsPath = [resourcePath stringByAppendingPathComponent:@"pruebaPDF3.pdf"];
    [bbScaner getXmlFromPDFWithPath:documentsPath finalName:@"finalPDF3" completion:^(NSString * result) {
        NSLog(@"result = %@",result);
    }];
    */
    
    //Borrar el array de fotos en local
    /*
    for (int i=0; i<[arrPicturesSaved count]; i++) {
        [UtilidadesImagen removeImage:[arrPicturesSaved objectAtIndex:i]];
    }
    arrPicturesSaved = [Utilities getPartPicturesName];
    [collection reloadData];
     */
}
#pragma mark UIpicker

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [self statusBar:YES];
    [picker dismissViewControllerAnimated:NO completion:^{/* done */}];
    
}

- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    contador++;
    UIImage *image = [info objectForKey:
                      UIImagePickerControllerOriginalImage];
    
    image = [UtilidadesImagen imagenAjustada:image];
    [UtilidadesImagen saveImage:image imagen:[NSString stringWithFormat:@"BBphoto%li", (long)contador]];
    [picker dismissViewControllerAnimated:NO completion:^{/* done */}];
    
    [collection reloadData];
}

- (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


-(void)statusBar:(BOOL)status
{
    [[UIApplication sharedApplication] setStatusBarHidden:status];
}


#pragma mark CollectionVIew

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    arrPicturesSaved = [Utilities getPartPicturesName];
    
    return [arrPicturesSaved count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    cellImagenes *cell = [cv dequeueReusableCellWithReuseIdentifier:@"cellImagenes" forIndexPath:indexPath];
    cell.delegate=self;
    cell.imgCelda.image = [UtilidadesImagen loadImage:[arrPicturesSaved objectAtIndex:indexPath.row]];
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(200, 200);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,0,0,0);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)deleteAction:(id)sender{
    NSLog(@"delete action!");
}
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
    indice=indexPath.row;
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action
    forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    
    if (action == @selector(deleteAction:forCell:)) {
        return YES;
    }
    return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action    forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
    
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    NSLog(@"canPerformAction");
    // The selector(s) should match your UIMenuItem selector
    if (action == @selector(deleteAction:forCell:)) {
        return YES;
    }
    return NO;
}

- (void)deleteAction:(id)sender forCell:(cellImagenes *)cell{
    [UtilidadesImagen removeImage:[arrPicturesSaved objectAtIndex:indice]];
    arrPicturesSaved = [Utilities getPartPicturesName];
    [collection reloadData];
}
#pragma mark Callbacks BBSCoreViewer
- (void)didFinishWithError:(NSError *)exitError{
    
}
- (void)didFinishWithChangesInScore:(BOOL)hasChangedScore andCustomization:(BOOL) hasChangedCustomization{
    
}
- (void)didReceiveMonitoringEvent: (NSDictionary *) monitoringEvent{
    for (NSString *aKey in monitoringEvent.allKeys)
        NSLog(@"key = %@, value = %@", aKey, [monitoringEvent valueForKey:aKey]);
}

@end
